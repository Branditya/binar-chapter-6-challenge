package id.branditya.ch6challengebinar.model


import com.google.gson.annotations.SerializedName

data class DetailMovieGenre(
    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: String
)