package id.branditya.ch6challengebinar.model

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}
