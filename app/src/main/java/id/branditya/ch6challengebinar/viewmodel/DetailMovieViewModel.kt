package id.branditya.ch6challengebinar.viewmodel

import android.content.Context
import androidx.lifecycle.*
import id.branditya.ch6challengebinar.database.FavoriteMovie
import id.branditya.ch6challengebinar.helper.AccountRepo
import id.branditya.ch6challengebinar.helper.MovieRepo
import id.branditya.ch6challengebinar.model.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class DetailMovieViewModel(context: Context) : ViewModel() {
    private val movieRepo: MovieRepo by lazy { MovieRepo() }
    private val accountRepo: AccountRepo by lazy { AccountRepo(context) }

    private var _favoriteMovieAdded = MutableLiveData<Boolean>()
    val favoriteMovieAdded: LiveData<Boolean> get() = _favoriteMovieAdded

    fun insertFavoriteMovie(movieId: Int, accountId: Int) {
        val favoriteMovie = FavoriteMovie(null, accountId, movieId)
        viewModelScope.launch {
            val result = accountRepo.insertFavoriteMovie(favoriteMovie)
            if (result != 0L) {
                _favoriteMovieAdded.postValue(true)
            }
        }
    }

    fun getDataDetailMovieFromNetwork(movieId: Int) = liveData(Dispatchers.IO) {
        emit(Resource.loading(null))
        try {
            emit(Resource.success(movieRepo.getDataDetailMovieFromNetwork(movieId)))
        } catch (e: Exception) {
            emit(Resource.error(data = null, message = e.message ?: "Error Occurred!"))
        }

    }
}