package id.branditya.ch6challengebinar.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.branditya.ch6challengebinar.helper.AccountRepo
import id.branditya.ch6challengebinar.helper.MovieRepo
import kotlinx.coroutines.launch

//Masih belum
class FavoriteMovieViewModel(context: Context) : ViewModel() {
    private val movieRepo: MovieRepo by lazy { MovieRepo() }
    private val accountRepo: AccountRepo by lazy { AccountRepo(context) }

    fun getData(accountId: Int) {
        viewModelScope.launch {
            val movieId = accountRepo.getFavoriteMovieList(accountId)
            if (movieId != null) {
                for (id in movieId) {

                }
            }
            movieRepo.getDataListMovieFromNetwork()
        }
    }
}