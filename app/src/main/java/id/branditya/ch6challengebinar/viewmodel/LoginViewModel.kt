package id.branditya.ch6challengebinar.viewmodel

import android.content.Context
import androidx.lifecycle.*
import id.branditya.ch6challengebinar.helper.AccountDataStoreManager
import id.branditya.ch6challengebinar.helper.AccountRepo
import kotlinx.coroutines.launch

class LoginViewModel(context: Context) : ViewModel() {
    private val accountRepo: AccountRepo by lazy { AccountRepo(context) }
    private val pref: AccountDataStoreManager by lazy { AccountDataStoreManager(context) }

    private var _accountRegistered = MutableLiveData<Boolean>()
    val accountRegistered: LiveData<Boolean> get() = _accountRegistered

    private fun setPref(accountIdPref: Int, usernamePref: String, emailPref: String) {
        viewModelScope.launch {
            pref.setDataPrefAccount(accountIdPref, usernamePref, emailPref)
        }
    }

    fun getLoginStatusPref(): LiveData<Boolean> {
        return pref.getLoginStatus().asLiveData()
    }

    fun loginAction(email: String?, password: String?) {
        viewModelScope.launch {
            val result = accountRepo.getRegisteredAccountForLogin(email, password)
            if (!result.isNullOrEmpty()) {
                val accountIdPref = result[0].id!!
                val usernamePref = result[0].username
                val emailPref = result[0].email
                setPref(accountIdPref, usernamePref, emailPref)
                _accountRegistered.postValue(true)
            } else if (email != null && password != null) {
                _accountRegistered.postValue(false)
            }
        }
    }
}