package id.branditya.ch6challengebinar

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.provider.Settings
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import id.branditya.ch6challengebinar.HomeFragment.Companion.ACCOUNT_ID_KEY
import id.branditya.ch6challengebinar.databinding.FragmentProfileBinding
import id.branditya.ch6challengebinar.helper.viewModelsFactory
import id.branditya.ch6challengebinar.viewmodel.ProfileViewModel
import java.io.ByteArrayOutputStream

class ProfileFragment : Fragment() {
    private var _binding: FragmentProfileBinding? = null
    private val binding get() = _binding!!

    private val viewModel: ProfileViewModel by viewModelsFactory { ProfileViewModel(requireContext()) }

    var accountId: Int = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentProfileBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        accountId = arguments?.getInt(ACCOUNT_ID_KEY, 0)!!
        viewModel.getProfileData(accountId)
        viewModel.getProfileImage(accountId)
        backButtonPressed()
        observeData()
        btnUpdateClicked()
        btnLogoutClicked()
        btnAddProfileImageClicked()
        observeImage()
    }

    private fun btnAddProfileImageClicked() {
        binding.btnAddProfileImage.setOnClickListener {
            checkingPermissions()
        }
    }

    private fun observeImage() {
        viewModel.profileImage.observe(viewLifecycleOwner) {
            if (!it.isNullOrEmpty()) {
                val index = it.substring(0, 1)
                when (index) {
                    "1" -> {
                        val data = it.substringAfter("_")
                        val uri = Uri.parse(data)
                        binding.ivProfileImage.setImageURI(uri)
                    }
                    "2" -> {
                        val data = it.substringAfter("_")
                        val bitmap = stringToBitMap(data)
                        binding.ivProfileImage.setImageBitmap(bitmap)
                    }
                }
            }
        }
    }

    private fun checkingPermissions() {
        // apakah permission sudah di setujui atau belum
        if (isGranted(
                requireActivity(),
                Manifest.permission.CAMERA,
                arrayOf(
                    Manifest.permission.CAMERA,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ),
                REQUEST_CODE_PERMISSION,
            )
        ) {
            chooseImageDialog()
        }
    }

    private fun isGranted(
        activity: Activity,
        permission: String,
        permissions: Array<String>,
        request: Int,
    ): Boolean {
        val permissionCheck = ActivityCompat.checkSelfPermission(activity, permission)
        return if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            // klau udah di tolak sebelumnya
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
                showPermissionDeniedDialog()
            }
            // klau belum pernah di tolak (request pertama kali)
            else {
                ActivityCompat.requestPermissions(activity, permissions, request)
            }
            false
        } else {
            true
        }
    }

    // dialoag yg muncul kalau user menolak permission yg di butuhkan
    private fun showPermissionDeniedDialog() {
        AlertDialog.Builder(requireContext())
            .setTitle("Permission Denied")
            .setMessage("Permission is denied, Please allow permissions from App Settings.")
            .setPositiveButton(
                "App Settings"
            ) { _, _ ->
                // mengarahkan user untuk buka halaman setting
                val intent = Intent()
                intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                val uri = Uri.fromParts("package", requireActivity().packageName, null)
                intent.data = uri
                startActivity(intent)
            }
            .setNegativeButton("Cancel") { dialog, _ -> dialog.cancel() }
            .show()
    }

    private fun chooseImageDialog() {
        AlertDialog.Builder(requireContext())
            .setMessage("Pilih Gambar")
            .setPositiveButton("Gallery") { _, _ -> openGallery() }
            .setNegativeButton("Camera") { _, _ -> openCamera() }
            .show()
    }

    // buat dapetin URI image gallery
    private val galleryResult =
        registerForActivityResult(ActivityResultContracts.GetContent()) { result ->
            if (result != null) {
                // munculin image dari gallery ke ImageView
                var uriString = result.toString()
                uriString = "1_$uriString"
                viewModel.saveProfileImage(accountId, uriString)
                binding.ivProfileImage.setImageURI(result)
            }
        }

    // buat buka gallery
    private fun openGallery() {
        requireActivity().intent.type = "image/*"
        galleryResult.launch("image/*")
    }

    // buat dapetin bitmap image dari camera
    private val cameraResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK && result.data != null) {
                // dapetin data bitmap dari intent
                val bitmap = result.data!!.extras?.get("data") as Bitmap

                // simpan bitmap ke dlm data store
                var bitmapString = bitMapToString(bitmap)
                bitmapString = "2_$bitmapString"
                viewModel.saveProfileImage(accountId, bitmapString)
                // load bitmap ke dalam imageView
                binding.ivProfileImage.setImageBitmap(bitmap)

            }
        }


    private fun bitMapToString(bitmap: Bitmap): String? {
        val baos = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos)
        val b: ByteArray = baos.toByteArray()
        return Base64.encodeToString(b, Base64.DEFAULT)
    }

    private fun stringToBitMap(encodedString: String?): Bitmap? {
        return try {
            val encodeByte =
                Base64.decode(encodedString, Base64.DEFAULT)
            BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.size)
        } catch (e: Exception) {
            e.message
            null
        }
    }

    // buat open camera
    private fun openCamera() {
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraResult.launch(cameraIntent)
    }


    private fun backButtonPressed() {
        binding.ivBackArrow.setOnClickListener {
            profileToHomeFragment()
        }
    }

    private fun observeData() {
        viewModel.detailAccount.observe(viewLifecycleOwner) {
            binding.apply {
                etUsername.setText(it.username)
                etFullname.setText(it.fullname)
                etBirthdate.setText(it.birthdate)
                etAddress.setText(it.address)
            }
        }
        viewModel.updatedData.observe(viewLifecycleOwner) {
            if (it == true) {
                createToast("Berhasil Update Profil").show()
                profileToHomeFragment()
            } else {
                createToast("Berhasil Gagal Profil").show()
            }
        }
    }

    private fun btnUpdateClicked() {
        binding.apply {
            btnUpdateProfile.setOnClickListener {
                val username = etUsername.text.toString()
                val fullname = etFullname.text.toString()
                val birthdate = etBirthdate.text.toString()
                val address = etAddress.text.toString()
                when {
                    username.isEmpty() -> {
                        etUsername.error = "Username harus diisi"
                    }
                    fullname.isEmpty() -> {
                        etFullname.error = "Email harus diisi"
                    }
                    else -> {
                        viewModel.updateData(accountId, username, fullname, birthdate, address)
                    }
                }
            }
        }
    }

    private fun profileToHomeFragment() {
        findNavController().popBackStack()
    }

    private fun btnLogoutClicked() {
        binding.btnLogout.setOnClickListener {
            logoutAction()
        }
    }

    private fun logoutAction() {
        viewModel.clearDataPrefAccount()
        createToast("Logout Berhasil").show()
        findNavController().navigate(R.id.action_profileFragment_to_loginFragment)
    }

    private fun createToast(message: String): Toast {
        return Toast.makeText(requireContext(), message, Toast.LENGTH_LONG)
    }

    companion object {
        const val REQUEST_CODE_PERMISSION = 100
    }
}