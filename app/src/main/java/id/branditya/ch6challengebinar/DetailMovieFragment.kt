package id.branditya.ch6challengebinar

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import id.branditya.ch6challengebinar.HomeFragment.Companion.ACCOUNT_ID_KEY
import id.branditya.ch6challengebinar.HomeFragment.Companion.MOVIE_ID_KEY
import id.branditya.ch6challengebinar.databinding.FragmentDetailMovieBinding
import id.branditya.ch6challengebinar.helper.toDateDMY
import id.branditya.ch6challengebinar.helper.viewModelsFactory
import id.branditya.ch6challengebinar.model.Status
import id.branditya.ch6challengebinar.viewmodel.DetailMovieViewModel

class DetailMovieFragment : Fragment() {
    private var _binding: FragmentDetailMovieBinding? = null
    private val binding get() = _binding!!

    private val viewModel: DetailMovieViewModel by viewModelsFactory {
        DetailMovieViewModel(requireContext())
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentDetailMovieBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val movieId = arguments?.getInt(MOVIE_ID_KEY, 0)
        observeData(movieId!!)
        favoriteButtonClicked()
        backButtonPressed()
    }

    private fun observeData(movieId: Int) {
        viewModel.getDataDetailMovieFromNetwork(movieId).observe(viewLifecycleOwner) {
            when (it.status) {
                Status.LOADING -> {
                    binding.pbDetailMovie.isVisible = true
                }
                Status.SUCCESS -> {
                    binding.pbDetailMovie.isVisible = false
                    it.data?.let { _ ->
                        binding.apply {
                            tvTitleMovie.text = it.data.title
                            tvTaglineMovie.text = it.data.tagline
                            tvOverviewContent.text = it.data.overview
                            tvReleaseDate.text = it.data.releaseDate.toDateDMY()
                            tvOriginalTitleContent.text = it.data.originalTitle
                            tvStatusContent.text = it.data.status

                            val genres = it.data.detailMovieGenres
                            val listGenre = mutableListOf<String>()
                            for (index in genres.indices) {
                                val genreName = genres[index].name
                                listGenre.add(genreName)
                            }
                            tvGenre.text = listGenre.joinToString(", ")

                            val runtime = it.data.runtime
                            val hour = runtime / 60
                            val minute = runtime - (hour * 60)
                            if (hour == 0) {
                                tvRuntimeHourValue.visibility = View.GONE
                                tvRuntimeHourInfo.visibility = View.GONE
                            } else {
                                tvRuntimeHourValue.text = hour.toString()
                            }
                            tvRuntimeMinuteValue.text = minute.toString()

                            val posterLink = it.data.posterPath
                            Glide.with(requireContext())
                                .load("https://image.tmdb.org/t/p/original$posterLink")
                                .into(binding.ivMovieImage)

                            svMovie.visibility = View.VISIBLE
                            pbDetailMovie.visibility = View.GONE
                        }
                    }
                }
                Status.ERROR -> {
                    binding.pbDetailMovie.isVisible = false
                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
                }
            }
        }
        viewModel.favoriteMovieAdded.observe(viewLifecycleOwner) {
            if (it) {
                Toast.makeText(
                    requireContext(),
                    "Berhasil Menambahkan ke Favorit",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                Toast.makeText(requireContext(), "Gagal Menambahkan ke Favorit", Toast.LENGTH_SHORT)
                    .show()
            }
        }
    }

    private fun favoriteButtonClicked() {
        binding.fabAddFavorite.setOnClickListener {
            val movieId = arguments?.getInt(MOVIE_ID_KEY, 0)
            val accountId = arguments?.getInt(ACCOUNT_ID_KEY, 0)
            viewModel.insertFavoriteMovie(movieId!!, accountId!!)
        }
    }

    private fun backButtonPressed() {
        binding.ivBackArrow.setOnClickListener {
            findNavController().popBackStack()
        }
    }
}