package id.branditya.ch6challengebinar

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import id.branditya.ch6challengebinar.databinding.FragmentLoginBinding
import id.branditya.ch6challengebinar.helper.viewModelsFactory
import id.branditya.ch6challengebinar.viewmodel.LoginViewModel

class LoginFragment : Fragment() {
    private var _binding: FragmentLoginBinding? = null
    private val binding get() = _binding!!

    private val viewModel: LoginViewModel by viewModelsFactory { LoginViewModel(requireContext()) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tvToRegisterClicked()
        btnLoginClicked()
        observeData()
    }

    private fun btnLoginClicked() {
        binding.apply {
            btnLogin.setOnClickListener {
                val etEmail = etEmail.text.toString()
                val etPassword = etPassword.text.toString()
                if (etEmail.isEmpty()) {
                    tilEmail.error = "Email harus diisi"
                } else {
                    tilEmail.error = null
                }
                if (etPassword.isEmpty()) {
                    tilPassword.error = "Password harus diisi"
                } else {
                    tilPassword.error = null
                }
                if (etEmail.isNotEmpty() && etPassword.isNotEmpty()) {
                    viewModel.loginAction(etEmail, etPassword)
                }
            }
        }
    }

    private fun observeData() {
        viewModel.accountRegistered.observe(viewLifecycleOwner) {
            if (it == true) {
                createToast("Login Berhasil").show()
                loginToHomeFragment()
            } else {
                createToast("Email atau Password Salah").show()
            }
        }
    }

    private fun loginToHomeFragment() {
        findNavController().navigate(R.id.action_loginFragment_to_homeFragment)
    }

    private fun tvToRegisterClicked() {
        binding.apply {
            tvToRegister.setOnClickListener {
                binding.etEmail.text?.clear()
                binding.etPassword.text?.clear()
                it.findNavController().navigate(R.id.action_loginFragment_to_registerFragment)
            }
        }
    }

    private fun createToast(message: String): Toast {
        return Toast.makeText(requireContext(), message, Toast.LENGTH_LONG)
    }
}