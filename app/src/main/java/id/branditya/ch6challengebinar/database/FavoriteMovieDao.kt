package id.branditya.ch6challengebinar.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query

@Dao
interface FavoriteMovieDao {
    @Query("SELECT * FROM FavoriteMovie WHERE accountId = :accountId")
    fun getFavoriteMovieList(accountId: Int): List<FavoriteMovie>

    @Insert(onConflict = REPLACE)
    fun insertFavorite(favoriteMovie: FavoriteMovie): Long
}