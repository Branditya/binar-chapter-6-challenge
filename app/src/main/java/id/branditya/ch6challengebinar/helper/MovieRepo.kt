package id.branditya.ch6challengebinar.helper

import id.branditya.ch6challengebinar.BuildConfig
import id.branditya.ch6challengebinar.service.ApiClient
import id.branditya.ch6challengebinar.service.ApiService

class MovieRepo {
    private val apiService: ApiService by lazy { ApiClient.instance }

    suspend fun getDataDetailMovieFromNetwork(movieId: Int) =
        apiService.getDetailMovie(movieId, BuildConfig.API_KEY)

    suspend fun getDataListMovieFromNetwork() = apiService.getPopularMovie(BuildConfig.API_KEY)
}