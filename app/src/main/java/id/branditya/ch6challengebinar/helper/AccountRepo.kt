package id.branditya.ch6challengebinar.helper

import android.content.Context
import id.branditya.ch6challengebinar.database.Account
import id.branditya.ch6challengebinar.database.AccountDatabase
import id.branditya.ch6challengebinar.database.FavoriteMovie
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class AccountRepo(context: Context) {
    private val mDb = AccountDatabase.getInstance(context)

    suspend fun getRegisteredAccountForLogin(email: String?, password: String?) =
        withContext(Dispatchers.IO) {
            mDb?.accountDao()?.getRegisteredAccountForLogin(email, password)
        }

    suspend fun getRegisteredAccountWithEmail(email: String?) = withContext(Dispatchers.IO) {
        mDb?.accountDao()?.getRegisteredAccountWithEmail(email)
    }

    suspend fun getRegisteredAccountWithId(id: Int) = withContext(Dispatchers.IO) {
        mDb?.accountDao()?.getRegisteredAccountWithId(id)
    }

    suspend fun insertAccount(account: Account) = withContext(Dispatchers.IO) {
        mDb?.accountDao()?.insertAccount(account)
    }

    suspend fun updateProfileAccount(
        id: Int,
        username: String,
        fullname: String,
        birthdate: String,
        address: String
    ) = withContext(Dispatchers.IO) {
        mDb?.accountDao()?.updateProfileAccount(id, username, fullname, birthdate, address)
    }

    suspend fun updateProfileImage(accountId: Int, profileImage: String) =
        withContext(Dispatchers.IO) {
            mDb?.accountDao()?.updateProfileImage(accountId, profileImage)
        }

    suspend fun insertFavoriteMovie(favoriteMovie: FavoriteMovie) = withContext(Dispatchers.IO) {
        mDb?.favoriteMovieDao()?.insertFavorite(favoriteMovie)
    }

    suspend fun getFavoriteMovieList(accountId: Int) = withContext(Dispatchers.IO) {
        mDb?.favoriteMovieDao()?.getFavoriteMovieList(accountId)
    }
}